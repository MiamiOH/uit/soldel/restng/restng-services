<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:20 AM
 */

return [
    'resources' => [
        'authentication' => [
            \MiamiOH\RESTng\Service\Resources\AuthenticationResourceProvider::class,
        ],
        'authorization' => [
            \MiamiOH\RESTng\Service\Resources\AuthorizationResourceProvider::class,
        ],
        'config' => [
            \MiamiOH\RESTng\Service\Resources\ConfigResourceProvider::class,
        ],
        'random' => [
            \MiamiOH\RESTng\Service\Resources\RandomResourceProvider::class,
        ],
    ]
];