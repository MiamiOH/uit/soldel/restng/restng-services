<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/8/18
 * Time: 7:51 PM
 */

namespace Tests\Feature;


use Tests\TestCase;

class RandomTest extends TestCase
{
    public function testCanGetRandomNumber(): void
    {
        $response = $this->getJson('/random/string');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'string'
                ],
            ]);

    }
}