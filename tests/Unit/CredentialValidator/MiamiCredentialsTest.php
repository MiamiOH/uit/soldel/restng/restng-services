<?php

namespace Tests\Unit\CredentialValidator;

use MiamiOH\RESTng\Service\CredentialValidator\MiamiCredentials;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;
use Tests\TestCase;

class MiamiCredentialsTest extends TestCase
{
    /** @var MiamiCredentials */
    private $validator;

    /** @var App|\PHPUnit_Framework_MockObject_MockObject */
    private $restngApp;

    public function setUp(): void
    {
        $this->restngApp = $this->createMock(App::class);

        $this->validator = new MiamiCredentials();

        $this->validator->setApp($this->restngApp);
    }

    public function testUsesAuthenticationResourceToValidateToken(): void
    {
        $this->willValidateTokenForUser('abc123');

        $response = $this->validator->validateToken('abc123');

        $this->assertValidResponse($response);
    }

    public function testReturnsValidResponseForValidToken(): void
    {
        $this->willValidateTokenForUser('abc123', 'doej');

        $response = $this->validator->validateToken('abc123');

        $this->assertEquals('doej', $response['username']);
    }

    public function testReturnsNotValidResponseForNotValidToken(): void
    {
        $this->willNotValidateTokenForUser('abc123');

        $response = $this->validator->validateToken('abc123');

        $this->assertNotValidResponse($response);
    }

    public function testUsesAuthenticationResourceToValidateUsernameAndPassword(): void
    {
        $this->willCreateTokenForUser('abc123', 'doej', 'donttell');

        $response = $this->validator->validateUsernamePassword('doej', 'donttell');

        $this->assertValidResponse($response);
    }

    public function testReturnsValidResponseForValidUsernameAndPassword(): void
    {
        $this->willCreateTokenForUser('abc123', 'doej', 'donttell');

        $response = $this->validator->validateUsernamePassword('doej', 'donttell');

        $this->assertEquals('doej', $response['username']);
        $this->assertEquals('abc123', $response['token']);
    }

    public function testReturnsNotValidResponseForNotValidUsernameAndPassword(): void
    {
        $this->willNotCreateTokenForUser('doej', 'donttell');

        $response = $this->validator->validateUsernamePassword('doej', 'donttell');

        $this->assertNotValidResponse($response);
    }

    private function assertValidResponse(array $response): void
    {
        $this->assertTrue($response['valid']);
    }

    private function assertNotValidResponse(array $response): void
    {
        $this->assertFalse($response['valid']);
    }

    private function willCreateTokenForUser(string $token, string $username = 'bob', string $password = 'secret'): void
    {
        $resourceResponse = new Response();
        $resourceResponse->setStatus(App::API_CREATED);
        $resourceResponse->setPayload([
            'username' => $username,
            'token' => $token,
        ]);

        $this->restngApp->method('callResource')
            ->with(
                $this->equalTo('authentication.v1.create'),
                $this->callback(function (array $arguments) use ($username, $password) {
                    $this->assertEquals($username, $arguments['data']['username']);
                    $this->assertEquals($password, $arguments['data']['password']);
                    return true;
                })
            )
            ->willReturn($resourceResponse);
    }

    private function willNotCreateTokenForUser(string $username = 'bob', string $password = 'secret'): void
    {
        $resourceResponse = new Response();
        $resourceResponse->setStatus(App::API_UNAUTHORIZED);
        $resourceResponse->setPayload([
            'username' => $username,
            'token' => null,
        ]);

        $this->restngApp->method('callResource')
            ->with(
                $this->equalTo('authentication.v1.create'),
                $this->callback(function (array $arguments) use ($username, $password) {
                    $this->assertEquals($username, $arguments['data']['username']);
                    $this->assertEquals($password, $arguments['data']['password']);
                    return true;
                })
            )
            ->willReturn($resourceResponse);
    }

    private function willValidateTokenForUser(string $token, string $username = 'bob'): void
    {
        $resourceResponse = new Response();
        $resourceResponse->setStatus(App::API_OK);
        $resourceResponse->setPayload([
            'username' => $username
        ]);

        $this->restngApp->method('callResource')
            ->with(
                $this->equalTo('authentication.v1.validate'),
                $this->callback(function (array $arguments) use ($token) {
                    $this->assertEquals($token, $arguments['params']['token']);
                    return true;
                })
            )
            ->willReturn($resourceResponse);
    }

    private function willNotValidateTokenForUser(string $token, string $username = 'bob'): void
    {
        $resourceResponse = new Response();
        $resourceResponse->setStatus(App::API_NOTFOUND);

        $this->restngApp->method('callResource')
            ->with(
                $this->equalTo('authentication.v1.validate'),
                $this->callback(function (array $arguments) use ($token) {
                    $this->assertEquals($token, $arguments['params']['token']);
                    return true;
                })
            )
            ->willReturn($resourceResponse);
    }

}
