<?php

namespace Tests\Unit\AuthorizationValidator;

use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationManager;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationSpecification;
use MiamiOH\RESTng\Util\Response;
use Tests\TestCase;

class AuthorizationManagerTest extends TestCase
{
    /** @var AuthorizationManager */
    private $validator;

    /** @var App|\PHPUnit_Framework_MockObject_MockObject */
    private $restngApp;

    public function setUp(): void
    {
        $this->restngApp = $this->createMock(App::class);

        $this->validator = new AuthorizationManager();

        $this->validator->setApp($this->restngApp);
    }

    public function testUsesAuthorizationServiceToValidateAuthorization(): void
    {
        $this->willValidateAuthorizationForUser('MyApp', 'TheModule', 'admin', 'doej');

        $response = $this->validator->validateAuthorizationForKey(
            AuthorizationSpecification::fromValues('MyApp', 'TheModule', 'admin'),
            'doej'
        );

        $this->assertIsAuthorized($response);
    }

    private function assertIsAuthorized(bool $response): void
    {
        $this->assertTrue($response);
    }

    private function assertNotIsAuthorized(bool $response): void
    {
        $this->assertFalse($response);
    }

    private function willValidateAuthorizationForUser(string $application, string $module, string $key, string $username): void
    {
        $resourceResponse = new Response();
        $resourceResponse->setStatus(App::API_OK);
        $resourceResponse->setPayload([
            'username' => $username
        ]);

        $this->restngApp->method('callResource')
            ->with(
                $this->equalTo('authorization.v1.check'),
                $this->callback(function (array $arguments) use ($application, $module, $key, $username) {
                    $this->assertEquals($application, $arguments['params']['application']);
                    $this->assertEquals($module, $arguments['params']['module']);
                    $this->assertEquals($key, $arguments['params']['key']);
                    $this->assertEquals($username, $arguments['options']['username']);
                    return true;
                })
            )
            ->willReturn($resourceResponse);
    }

}
