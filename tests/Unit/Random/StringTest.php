<?php

namespace Tests\Unit\Random;

use MiamiOH\RESTng\App;

class StringTest extends \MiamiOH\RESTng\Testing\TestCase
{
  private $randomString;

  protected function setUp(): void
  {
    parent::setUp();

    $app = $this->getMockBuilder(App::class)
          ->setMethods(array('newResponse', 'locationFor'))
          ->getMock();

    $app->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());
    $app->method('locationFor')
      ->with($this->callback(array($this, 'locationForNameWith')), $this->callback(array($this, 'locationForParamsWith')))
      ->will($this->returnCallback(array($this, 'locationForMock')));

    $this->randomString = new \MiamiOH\RESTng\Service\Random\Str();

    $this->randomString->setApp($app);

  }

  public function testGetString() {

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array());

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertTrue(array_key_exists('string', $payload));

  }

  public function testGetStringMinimumLengthNotNumeric() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('minimumLength must be numeric');

      $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $minLength = 'n100';
    $request->expects($this->once())->method('getOptions')->willReturn(array('minimumLength' => $minLength));

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

  }

  public function testGetStringMaximumLengthNotNumeric() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('maximumLength must be numeric');

      $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $minLength = 'n100';
    $request->expects($this->once())->method('getOptions')->willReturn(array('maximumLength' => $minLength));

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

  }

  public function testGetStringMinimumLengthGreaterThanMaximumLength() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('minimumLength cannot be greater than maximumLength');

      $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $minLength = 100;
    $request->expects($this->once())->method('getOptions')->willReturn(array('minimumLength' => $minLength));

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

  }

  public function testGetStringMinimumLengthNegative() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('minimumLength must be greater than zero');

      $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $minLength = -1;
    $request->expects($this->once())->method('getOptions')->willReturn(array('minimumLength' => $minLength));

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

  }

  public function testGetStringMaximumLengthExceeded() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('maximumLength cannot be greater than 1000');

      $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $maxLength = 10000;
    $request->expects($this->once())->method('getOptions')->willReturn(array('maximumLength' => $maxLength));

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

  }

  public function testGetStringMinimumAndMaximumLength() {

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $minLength = 100;
    $maxLength = 110;
    $request->expects($this->once())->method('getOptions')->willReturn(
        array('minimumLength' => $minLength, 'maximumLength' => $maxLength)
      );

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertTrue(array_key_exists('string', $payload));
    $this->assertTrue(strlen($payload['string']) >= $minLength);
    $this->assertTrue(strlen($payload['string']) <= $maxLength);

  }

  public function testGetStringMinimumAndMaximumLengthSame() {

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $minLength = 100;
    $maxLength = 100;
    $request->expects($this->once())->method('getOptions')->willReturn(
        array('minimumLength' => $minLength, 'maximumLength' => $maxLength)
      );

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertTrue(array_key_exists('string', $payload));
    $this->assertTrue(strlen($payload['string']) === $minLength);
    $this->assertTrue(strlen($payload['string']) === $maxLength);

  }

  public function testGetStringAllowedCharactersUppercase() {

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $allowedCharacters = array('uppercase');
    $request->expects($this->once())->method('getOptions')->willReturn(
        array('allowedCharacters' => $allowedCharacters)
      );

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertTrue(array_key_exists('string', $payload));
    $this->assertTrue(preg_match('[^A-Z]', $payload['string']) === 0);

  }

  public function testGetStringAllowedCharactersLowercase() {

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $allowedCharacters = array('lowercase');
    $request->expects($this->once())->method('getOptions')->willReturn(
        array('allowedCharacters' => $allowedCharacters)
      );

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertTrue(array_key_exists('string', $payload));
    $this->assertTrue(preg_match('[^a-z]', $payload['string']) === 0);

  }

  public function testGetStringAllowedCharactersNumbers() {

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $allowedCharacters = array('numbers');
    $request->expects($this->once())->method('getOptions')->willReturn(
        array('allowedCharacters' => $allowedCharacters)
      );

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertTrue(array_key_exists('string', $payload));
    $this->assertTrue(preg_match('[^0-9]', $payload['string']) === 0);

  }

  public function testGetStringAllowedCharactersMultiple() {

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $allowedCharacters = array('uppercase', 'lowercase');
    $request->expects($this->once())->method('getOptions')->willReturn(
        array('allowedCharacters' => $allowedCharacters)
      );

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertTrue(array_key_exists('string', $payload));
    $this->assertTrue(preg_match('[^A-Za-z]', $payload['string']) === 0);

  }

  public function testGetStringAllowedCharactersInvalid() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('lowercas is an invalid value for allowedCharacters');

      $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $allowedCharacters = array('uppercase', 'lowercas');
    $request->expects($this->once())->method('getOptions')->willReturn(
        array('allowedCharacters' => $allowedCharacters)
      );

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

  }

  public function testGetStringAllowedCharactersNone() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('no character types specified');

      $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $allowedCharacters = array();
    $request->expects($this->once())->method('getOptions')->willReturn(
        array('allowedCharacters' => $allowedCharacters)
      );

    $this->randomString->setRequest($request);

    $resp = $this->randomString->getString();

  }

  public function locationForNameWith($subject) {
    $this->locationForNameBeingCalled = $subject;
    return true;
  }

  public function locationForParamsWith($subject) {
    $this->locationForParamsBeingCalled = $subject;
    return true;
  }

  public function locationForMock() {
    $path = str_replace('.', '/', $this->locationForNameBeingCalled);
    $params = $this->locationForParamsBeingCalled;
    $qsFunction = function ($key) use ($params) { return $key . '=' . $params[$key]; };
    $queryString = implode('&', array_map($qsFunction, array_keys($this->locationForParamsBeingCalled)));
    return 'http://example.com/' . $path . ($queryString ? '?' . $queryString : '');
  }

}
