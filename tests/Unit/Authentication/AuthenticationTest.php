<?php

namespace Tests\Unit\Authentication;

use Carbon\Carbon;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\LDAPFactory;
use MiamiOH\RESTng\Util\Request;

class AuthenticationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $authentication;

    private $dbh;

    private $token = '';
    private $credentials = array();
    private $resourceBeingCalled = '';
    private $locationForNameBeingCalled = '';
    private $locationForParmsBeingCalled = array();
    private $ldapHandler;

    private $tokens = array(
        'abc123' => array(
            'token' => 'abc123',
            'username' => 'doej',
            'credentialSource' => 'ldap',
            'tokenLifetime' => '2020-01-01 23:59:59',
            'authenticationLocation' => '',
        ),
        'def123' => array(
            'token' => 'def123',
            'username' => 'doej',
            'credentialSource' => 'ldap',
            'tokenLifetime' => '2020-01-01 23:59:59',
            'authenticationLocation' => '',
        ),
    );

    private $localUsers = array(
        'doej' => array(
            'username' => 'doej',
            'algorithm' => 1,
            'credential_hash' => '25d55ad283aa400af464c76d713c07ad',
            'create_date' => '2014-01-01',
            'create_username' => 'admind',
            'expiration_date' => '2020-01-01',
            'comments' => '',
        ),
    );

    protected function setUp(): void
    {
        parent::setUp();

        $this->token = '';
        $this->credentials = array();
        $this->resourceBeingCalled = '';
        $this->locationForNameBeingCalled = '';
        $this->locationForParamsBeingCalled = array();

        $app = $this->getMockBuilder('\MiamiOH\RESTng\App')
            ->setMethods(array('newResponse', 'callResource', 'locationFor'))
            ->getMock();

        $app->method('callResource')
            ->with($this->callback(array($this, 'callResourceWith')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));
        $app->method('locationFor')
            ->with($this->callback(array($this, 'locationForNameWith')), $this->callback(array($this, 'locationForParamsWith')))
            ->will($this->returnCallback(array($this, 'locationForMock')));

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstrow_assoc', 'perform', 'getType', 'getErrorNum', 'getErrorString', 'queryall_array'))
            ->getMock();

        $this->dbh->type = 'MySQL';
        $this->dbh->error = '';
        $this->dbh->error_string = '';

        $this->dbh->method('getType')
            ->will($this->returnCallback(array($this, 'getTypeMock')));

        $this->dbh->method('getErrorNum')
            ->will($this->returnCallback(array($this, 'getErrorNumMock')));

        $this->dbh->method('getErrorString')
            ->will($this->returnCallback(array($this, 'getErrorStringMock')));

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $ldapFactory = $this->createMock(LDAPFactory::class);
        $this->ldapHandler = $this->createMock(\MiamiOH\RESTng\Legacy\LDAP::class);
        $ldapFactory->method('getHandle')->willReturn($this->ldapHandler);
        $db->method('getHandle')->willReturn($this->dbh);

        $this->authentication = new \MiamiOH\RESTng\Service\Authentication\Authentication();

        $this->authentication->setDatabase($db);
        $this->authentication->setApp($app);
        $this->authentication->setLdapFactory($ldapFactory);
    }

    public function testValidateToken()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $this->token = 'abc123';

        $this->dbh->method('queryFirstRow_assoc')
            ->with($this->callback(array($this, 'queryTokenSqlWith')), $this->callback(array($this, 'queryTokenValueWith')))
            ->will($this->returnCallback(array($this, 'queryTokenMock')));

        $request->expects($this->once())->method('getResourceParam')->willReturn($this->token);

        $this->authentication->setRequest($request);

        $resp = $this->authentication->validateToken();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals($this->tokens[$this->token], $payload);

    }

    public function testValidateTokenInvalid()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $this->token = 'xyz987';

        $this->dbh->method('queryFirstRow_assoc')
            ->with($this->callback(array($this, 'queryTokenSqlWith')), $this->callback(array($this, 'queryTokenValueWith')))
            ->will($this->returnCallback(array($this, 'queryTokenMock')));

        $request->expects($this->once())->method('getResourceParam')->willReturn($this->token);

        $this->authentication->setRequest($request);

        $resp = $this->authentication->validateToken();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());

    }

    public function testAuthenticateUserRequiresType()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('RESTng\Service\Authentication\Authentication::authenticateUser() requires type');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $this->credentials = array(
            'type' => '',
            'username' => 'doej',
            'password' => '12345678',
        );

        $request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        $this->authentication->setRequest($request);

        $resp = $this->authentication->authenticateUser();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

    public function testAuthenticateUserTypeUnknown()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('RESTng\Service\Authentication\Authentication::authenticateUser() type must be one of "usernamePassword" or "cas-proxy"');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $this->credentials = array(
            'type' => 'unknown',
            'username' => 'doej',
            'password' => '12345678',
        );

        $request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        $this->authentication->setRequest($request);

        $resp = $this->authentication->authenticateUser();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

    public function testAuthenticateUserUsernamePasswordRequiresUsername()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('RESTng\Service\Authentication\Authentication::authenticateUser() requires username for type usernamePassword');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $this->credentials = array(
            'type' => 'usernamePassword',
            'username' => '',
            'password' => '12345678',
        );

        $request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        $this->authentication->setRequest($request);

        $resp = $this->authentication->authenticateUser();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

    public function testAuthenticateUserUsernamePasswordRequiresPassword()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('RESTng\Service\Authentication\Authentication::authenticateUser() requires password for type usernamePassword');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $this->credentials = array(
            'type' => 'usernamePassword',
            'username' => 'doej',
            'password' => '',
        );

        $request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        $this->authentication->setRequest($request);

        $resp = $this->authentication->authenticateUser();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

    public function testAuthenticateUserUsernamePasswordInvalid()
    {

        $this->dbh->method('queryFirstRow_assoc')
            ->with($this->callback(array($this, 'queryLocalUserSqlWith')), $this->callback(array($this, 'queryLocalUserValueWith')))
            ->will($this->returnCallback(array($this, 'queryLocalUserMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $this->ldapHandler->method('authenticate')->willReturn(false);

        $this->credentials = array(
            'type' => 'usernamePassword',
            'username' => 'doej',
            'password' => '1234',
        );

        $request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        $this->authentication->setRequest($request);

        $resp = $this->authentication->authenticateUser();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());

    }

    public function testAuthenticateUserUsernamePasswordLocalUser()
    {

        $this->dbh->method('queryFirstRow_assoc')
            ->with($this->callback(array($this, 'queryLocalUserSqlWith')), $this->callback(array($this, 'queryLocalUserValueWith')))
            ->will($this->returnCallback(array($this, 'queryLocalUserMock')));

        // TODO - test that the perform is called correctly.
        $this->dbh->method('perform')->willReturn(true);

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $this->credentials = array(
            'type' => 'usernamePassword',
            'username' => 'doej',
            'password' => '12345678',
        );

        $request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        $this->authentication->setRequest($request);

        $resp = $this->authentication->authenticateUser();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(5, count(array_keys($payload)));
        $this->assertTrue(array_key_exists('token', $payload));
        $this->assertTrue(array_key_exists('authenticationLocation', $payload));
        $this->assertTrue(array_key_exists('username', $payload));
        $this->assertTrue(array_key_exists('credentialSource', $payload));
        $this->assertTrue(array_key_exists('tokenLifetime', $payload));

    }

    public function testReturnsCollectionOfLocalUsers(): void
    {
        $this->dbh->method('queryall_array')
            ->willReturn([$this->makeUserRecord()]);

        $request = $this->createMock(Request::class);

        $this->authentication->setRequest($request);

        $resp = $this->authentication->getUsers();

        $users = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertCount(1, $users);
    }

    public function testReturnsRequestedLocalUser(): void
    {
        $this->dbh->method('queryfirstrow_assoc')
            ->willReturn($this->makeUserRecord());

        $request = $this->createMock(Request::class);
        $request->method('getResourceParam')
            ->with('username')
            ->willReturn('ws_user');

        $this->authentication->setRequest($request);

        $resp = $this->authentication->getUser();

        $user = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals('ws_user', $user['username']);
    }

    public function testCreatesNewLocalUser(): void
    {
        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(function (string $query) {
                $this->assertStringContainsString('insert into ws_authentication_local_users', $query);
                return true;
            }),
            $this->callback(function (array $values) {
                $this->assertEquals('ws_test', $values[0]);
                $this->assertEquals(hash('md5', 'testme'), $values[2]);
                return true;
            }));

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->willReturn($this->makeUserRecord(['username' => 'ws_test']));

        $request = $this->createMock(Request::class);
        $request->method('getData')
            ->willReturn([
                'username' => 'ws_test',
                'password' => 'testme',
                'creatorId' => 'publicjq',
                'comments' => 'Used for testing'
            ]);

        $this->authentication->setRequest($request);

        $resp = $this->authentication->createUser();

        $user = $resp->getPayload();

        $this->assertEquals(App::API_CREATED, $resp->getStatus());
        $this->assertEquals('ws_test', $user['username']);
    }

    public function testUpdatesLocalUser(): void
    {
        $request = $this->createMock(Request::class);
        $request->method('getResourceParam')
            ->with('username')
            ->willReturn('ws_test');

        $request->method('getData')
            ->willReturn([
                'username' => 'ws_test',
                'password' => 'testme',
                'creatorId' => 'publicjq',
                'comments' => 'Used for testing',
                'expirationDate' => Carbon::now()->addYears(2)->format('Y-m-d'),
            ]);

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(function (string $query) {
                $this->assertStringContainsString('update ws_authentication_local_users set expiration_date', $query);
                return true;
            }),
            $this->callback(function (array $values) {
                $this->assertEquals(Carbon::now()->addYears(2)->format('Y-m-d'), $values[0]);
                return true;
            }));

        $this->authentication->setRequest($request);

        $resp = $this->authentication->updateUser();

        $this->assertEquals(App::API_OK, $resp->getStatus());
    }

    public function testUpdatesLocalUserPassword(): void
    {
        $request = $this->createMock(Request::class);
        $request->method('getResourceParam')
            ->with('username')
            ->willReturn('ws_test');

        $request->method('getData')
            ->willReturn([
                'username' => 'ws_test',
                'password' => 'testme2',
            ]);

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->willReturn($this->makeUserRecord(['username' => 'ws_test']));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(function (string $query) {
                $this->assertStringContainsString('update ws_authentication_local_users set credential_hash', $query);
                return true;
            }),
            $this->callback(function (array $values) {
                $this->assertEquals(hash('md5', 'testme2'), $values[0]);
                return true;
            }));

        $this->authentication->setRequest($request);

        $resp = $this->authentication->updateUserPassword();

        $this->assertEquals(App::API_OK, $resp->getStatus());
    }

    public function testDeletesLocalUser(): void
    {
        $request = $this->createMock(Request::class);
        $request->method('getResourceParam')
            ->with('username')
            ->willReturn('ws_user');

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(function (string $query) {
                $this->assertStringContainsString('delete from ws_authentication_local_users', $query);
                return true;
            }),
            $this->callback(function (string $value) {
                $this->assertEquals('ws_user', $value);
                return true;
            }));

        $this->authentication->setRequest($request);

        $resp = $this->authentication->deleteUser();

        $this->assertEquals(App::API_OK, $resp->getStatus());
    }

    private function makeUserRecord(array $overrides = []): array
    {
        $data = [
            'username' => 'ws_user',
            'create_date' => Carbon::now()->subYear()->format('Y-m-d'),
            'create_username' => 'publicjq',
            'expiration_date' => Carbon::now()->addYear()->format('Y-m-d'),
            'comments' => 'Used for testing',
            'algorithm' => 1,
        ];

        return array_merge($data, $overrides);
    }

    public function queryTokenSqlWith($subject)
    {
        // validate sql query?
        return true;
    }

    public function queryTokenValueWith($subject)
    {
        $this->assertEquals($this->token, $subject);
        return true;
    }

    public function queryTokenMock()
    {
        if (isset($this->tokens[$this->token])) {
            return $this->tokens[$this->token];
        } else {
            return \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET;
        }
    }

    public function getDataMock()
    {
        return $this->credentials;
    }

    public function queryLocalUserSqlWith($subject)
    {
        // validate sql query?
        return true;
    }

    public function queryLocalUserValueWith($subject)
    {
        $this->assertEquals($this->credentials['username'], $subject);
        return true;
    }

    public function queryLocalUserMock()
    {
        if (isset($this->localUsers[$this->credentials['username']])) {
            return $this->localUsers[$this->credentials['username']];
        } else {
            return \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET;
        }
    }

    public function callResourceWith($subject)
    {
        $this->resourceBeingCalled = $subject;
        return true;
    }

    public function callResourceMock()
    {
        if ($this->resourceBeingCalled === 'random.string') {
            $response = new \MiamiOH\RESTng\Util\Response();
            $response->setPayload(array('string' => 'abcdef123456'));
            return $response;
        }

        return null;
    }

    public function locationForNameWith($subject)
    {
        $this->locationForNameBeingCalled = $subject;
        return true;
    }

    public function locationForParamsWith($subject)
    {
        $this->locationForParamsBeingCalled = $subject;
        return true;
    }

    public function locationForMock()
    {
        $path = str_replace('.', '/', $this->locationForNameBeingCalled);
        $params = $this->locationForParamsBeingCalled;
        $qsFunction = function ($key) use ($params) {
            return $key . '=' . $params[$key];
        };
        $queryString = implode('&', array_map($qsFunction, array_keys($this->locationForParamsBeingCalled)));
        return 'http://example.com/' . $path . ($queryString ? '?' . $queryString : '');
    }

    public function getTypeMock()
    {
        return $this->dbh->type;
    }

    public function getErrorNumMock()
    {
        return $this->dbh->error;
    }

    public function getErrorStringMock()
    {
        return $this->dbh->error_string;
    }

}
