<?php

namespace Tests\Unit\Authentication;

class AuthenticationWsTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $authentication;

    private $restService;
    private $restClient;

    private $token = '';
    private $credentials = array();
    private $remoteResponseData = array();

    private $tokens = array(
        'abc123' => array(
            'token' => 'abc123',
            'username' => 'doej',
            'credentialSource' => 'ldap',
            'tokenLifetime' => '2020-01-01 23:59:59',
            'authenticationLocation' => '',
        ),
        'abc123&def' => array(
            'token' => 'abc123&def',
            'username' => 'doej',
            'credentialSource' => 'ldap',
            'tokenLifetime' => '2020-01-01 23:59:59',
            'authenticationLocation' => '',
        ),
    );

    protected function setUp(): void
    {
        parent::setUp();

        $this->token = '';
        $this->credentials = array();
        $this->remoteResponseData = array();

        $this->restService = $this->getMockBuilder('\MiamiOH\RESTng\Connector\RESTService\Mock')
            ->setMethods(array('newClient'))
            ->getMock();

        $this->restClient = $this->getMockBuilder('\MiamiOH\RESTng\Connector\RESTClient\Mock')
            ->setMethods(array('get', 'post'))
            ->getMock();

        $this->restResponse = $this->getMockBuilder('\MiamiOH\RESTng\Connector\RESTResponse\Mock')
            ->setMethods(array('getBody'))
            ->getMock();

        $this->restService->method('newClient')
            ->with($this->callback(array($this, 'newClientWith')))
            ->will($this->returnCallback(array($this, 'newClientMock')));

        $this->restClient->method('get')
            ->with($this->callback(array($this, 'getClientWith')))
            ->will($this->returnCallback(array($this, 'getClientMock')));

        $this->restClient->method('post')
            ->with($this->callback(array($this, 'postClientWith')))
            ->will($this->returnCallback(array($this, 'postClientMock')));

        $this->restResponse->method('getBody')
            ->will($this->returnCallback(array($this, 'getBodyMock')));

        $this->authentication = new \MiamiOH\RESTng\Service\Authentication\AuthenticationWS();

        $this->authentication->setRestService($this->restService);

    }

    public function testGetRestService()
    {

        $this->assertEquals($this->restService, $this->authentication->getRestService());

    }

    public function testAuthenticate()
    {

        $data = array(
            'type' => 'usernamePassword',
        );
        $this->remoteResponseData = array('status' => \MiamiOH\RESTng\App::API_OK, 'data' => array('username' => 'test'));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')
            ->willReturn($data);

        $this->authentication->setRequest($request);

        $resp = $this->authentication->authenticateUser();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

    public function testAuthenticateFailed()
    {

        $data = array(
            'type' => 'usernamePassword',
        );
        $this->remoteResponseData = array('status' => \MiamiOH\RESTng\App::API_UNAUTHORIZED, 'data' => array('username' => 'test'));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')
            ->willReturn($data);

        $this->authentication->setRequest($request);

        $resp = $this->authentication->authenticateUser();

        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());

    }

    public function testValidateTokenWS()
    {

        $this->token = 'abc123';
        $this->remoteResponseData = array('data' => $this->tokens[$this->token]);

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('token'))
            ->willReturn($this->token);

        $this->authentication->setRequest($request);

        $resp = $this->authentication->validateToken();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals($this->tokens[$this->token], $payload);

    }

    public function testValidateTokenWSUrlEncode()
    {

        $this->token = 'abc123&def';
        $this->remoteResponseData = array('data' => $this->tokens[$this->token]);

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('token'))
            ->willReturn($this->token);

        $this->authentication->setRequest($request);

        $resp = $this->authentication->validateToken();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals($this->tokens[$this->token], $payload);

    }

    public function newClientWith($subject)
    {
        $this->assertEquals(env('MU_API_URL', 'https://example.com'), $subject['base_uri']);
        return true;
    }

    public function newClientMock()
    {
        return $this->restClient;
    }

    public function getClientWith($subject)
    {
        #print "Subject $subject\n";
        return true;
    }

    public function getClientMock()
    {
        return $this->restResponse;
    }

    public function postClientWith($subject)
    {
        return true;
    }

    public function postClientMock()
    {
        return $this->restResponse;
    }

    public function getBodyMock()
    {
        return json_encode($this->remoteResponseData);
    }

    public function getUrlWith($subject)
    {
        $this->assertEquals('/authentication/' . urlencode($this->token), $subject);
        return true;
    }

    public function getUrlMock()
    {
        return array('data' => $this->tokens[$this->token]);
    }

}
