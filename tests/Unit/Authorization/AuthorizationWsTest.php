<?php

namespace Tests\Unit\Authorization;

class AuthorizationWsTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $authorization;

    private $restService;
    private $restClient;

    private $remoteResponseData = array();

    protected function setUp(): void
    {
        parent::setUp();

        $this->token = '';
        $this->credentials = array();
        $this->remoteResponseData = array();

        $this->restService = $this->getMockBuilder('\MiamiOH\RESTng\Connector\RESTService\Mock')
            ->setMethods(array('newClient'))
            ->getMock();

        $this->restClient = $this->getMockBuilder('\MiamiOH\RESTng\Connector\RESTClient\Mock')
            ->setMethods(array('get', 'post'))
            ->getMock();

        $this->restResponse = $this->getMockBuilder('\MiamiOH\RESTng\Connector\RESTResponse\Mock')
            ->setMethods(array('getBody'))
            ->getMock();

        $this->restService->method('newClient')
            ->with($this->callback(array($this, 'newClientWith')))
            ->will($this->returnCallback(array($this, 'newClientMock')));

        $this->restClient->method('get')
            ->with($this->callback(array($this, 'getClientWith')))
            ->will($this->returnCallback(array($this, 'getClientMock')));

        $this->restClient->method('post')
            ->with($this->callback(array($this, 'postClientWith')))
            ->will($this->returnCallback(array($this, 'postClientMock')));

        $this->restResponse->method('getBody')
            ->will($this->returnCallback(array($this, 'getBodyMock')));

        $this->authorization = new \MiamiOH\RESTng\Service\Authorization\AuthorizationWS();

        $this->authorization->setRestService($this->restService);

    }

    public function testGetRestService()
    {

        $this->assertEquals($this->restService, $this->authorization->getRestService());

    }

    public function testGetAuthorization()
    {

        $this->remoteResponseData = array(
            array('status' => \MiamiOH\RESTng\App::API_OK,
                                          'data' => array(
                                              'applicationName' => 'TestApplication',
                                              'module' => 'TestModule',
                                              'key' => 'testkey',
                                              'message' => '',
                                              'allowed' => 'true'
                                          ),
            )
        );

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $paramMap = [
            ['application', null, 'TestApplication'],
            ['module', null, 'TestModule'],
            ['key', null, 'testkey'],
        ];

        $request->expects($this->exactly(3))->method('getResourceParam')
            ->will($this->returnValueMap($paramMap));

        $request->expects($this->once())->method('getOptions')
            ->willReturn([ 'username' => 'testuser' ]);

        $this->authorization->setRequest($request);

        $resp = $this->authorization->getAuthorization();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

    public function testIsAuthorized()
    {

        $this->remoteResponseData = array(
            array('status' => \MiamiOH\RESTng\App::API_OK,
                                          'data' => array(
                                              'applicationName' => 'TestApplication',
                                              'module' => 'TestModule',
                                              'key' => 'testkey',
                                              'message' => '',
                                              'allowed' => 'true'
                                          ),
            )
        );

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $paramMap = [
            ['application', null, 'TestApplication'],
            ['module', null, 'TestModule'],
            ['key', null, 'testkey'],
        ];

        $request->expects($this->exactly(3))->method('getResourceParam')
            ->will($this->returnValueMap($paramMap));

        $request->expects($this->once())->method('getOptions')
            ->willReturn([ 'username' => 'testuser' ]);

        $this->authorization->setRequest($request);

        $resp = $this->authorization->isAuthorized();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

    public function testGetAuthorizations()
    {

        $this->remoteResponseData = array(
            array('status' => \MiamiOH\RESTng\App::API_OK,
                                          'data' => array(
                                              'applicationName' => 'TestApplication',
                                              'module' => 'TestModule',
                                              'key' => 'testkey1',
                                              'message' => '',
                                              'allowed' => 'true'
                                          ),
            ),
            array('status' => \MiamiOH\RESTng\App::API_OK,
                                          'data' => array(
                                              'applicationName' => 'TestApplication',
                                              'module' => 'TestModule',
                                              'key' => 'testkey2',
                                              'message' => '',
                                              'allowed' => 'true'
                                          ),
            )
        );

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $paramMap = [
            ['application', null, 'TestApplication'],
            ['module', null, 'TestModule'],
        ];

        $request->expects($this->exactly(2))->method('getResourceParam')
            ->will($this->returnValueMap($paramMap));

        $request->expects($this->once())->method('getOptions')
            ->willReturn([ 'username' => 'testuser', 'key' => ['view', 'update'] ]);

        $this->authorization->setRequest($request);

        $resp = $this->authorization->getAuthorizations();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $payload = $resp->getPayload();
        $this->assertCount(2, $payload);

    }

    public function newClientWith($subject)
    {
        $this->assertEquals(env('MU_API_URL', 'https://example.com'), $subject['base_uri']);
        return true;
    }

    public function newClientMock()
    {
        return $this->restClient;
    }

    public function getClientWith($subject)
    {
        #print "Subject $subject\n";
        return true;
    }

    public function getClientMock()
    {
        return $this->restResponse;
    }

    public function postClientWith($subject)
    {
        return true;
    }

    public function postClientMock()
    {
        return $this->restResponse;
    }

    public function getBodyMock()
    {
        return json_encode(array_shift($this->remoteResponseData));
    }

    public function getUrlWith($subject)
    {
        $this->assertEquals('/authentication/' . urlencode($this->token), $subject);
        return true;
    }

    public function getUrlMock()
    {
        return array('data' => $this->tokens[$this->token]);
    }

}
