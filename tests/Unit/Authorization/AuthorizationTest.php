<?php

class AuthorizationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $authorization;

    private $resourceParamName;
    private $resourceParamMap;

    private $currentQuery;
    private $queryParams;
    private $currentQueryMockResult = array();

    protected function setUp(): void
    {
        parent::setUp();

        $this->resourceParamName = '';
        $this->resourceParamMap = array();

        $this->currentQuery = '';
        $this->queryParams = array();
        $this->currentQueryMockResult = array();

        $app = $this->getMockBuilder('\MiamiOH\RESTng\App')
            ->setMethods(array('callResource', 'locationFor'))
            ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstcolumn', 'queryall_list'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $dsFactory = $this->getMockBuilder('\MiamiOH\RESTng\Connector\DataSourceFactory')
            ->setMethods(array('getDataSource'))
            ->getMock();

        $dsFactory->method('getDataSource')->willReturn(
            \MiamiOH\RESTng\Connector\DataSource::fromArray([
                'name' => 'test',
                'type' => 'LDAP',
            ])
        );

        $this->authorization = new \MiamiOH\RESTng\Service\Authorization\Authorization();

        $this->authorization->setApp($app);
        $this->authorization->setDatabase($db);
        $this->authorization->setDataSourceFactory($dsFactory);

    }

    public function testGetAuthorization() {

        $this->resourceParamMap = array(
            'application' => 'Test App',
            'module' => 'Test App Module',
            'key' => 'Test App Key',
        );

        $this->currentQueryMockResult['selectApplication'] = 1;
        $this->currentQueryMockResult['selectModule'] = 10;
        $this->currentQueryMockResult['selectEntityAP'] = 20;
        $this->currentQueryMockResult['selectDisabled'] = 0;
        $this->currentQueryMockResult['selectAuthorization'] = 1;

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->method('getResourceParam')
            ->with($this->callback(array($this, 'resourceParmsWith')))
            ->will($this->returnCallback(array($this, 'resourceParamsMock')));

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryColumnWithSql')),
                $this->callback(array($this, 'queryColumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryColumnMock')));

        $request->method('getOptions')->willReturn(array('username' => 'testuser'));

        $this->authorization->setRequest($request);

        $resp = $this->authorization->getAuthorization();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

    }

    public function testGetAuthorizations() {

        $this->resourceParamMap = array(
            'application' => 'Test App',
            'module' => 'Test App Module',
        );

        $keys = array('Test App Key 1', 'Test App Key 2');

        $this->currentQueryMockResult['selectApplication'] = 1;
        $this->currentQueryMockResult['selectModule'] = 10;
        $this->currentQueryMockResult['selectEntityAP'] = 20;
        $this->currentQueryMockResult['selectDisabled'] = 0;
        $this->currentQueryMockResult['selectAuthorization'] = 1;

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->method('getResourceParam')
            ->with($this->callback(array($this, 'resourceParmsWith')))
            ->will($this->returnCallback(array($this, 'resourceParamsMock')));

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryColumnWithSql')),
                $this->callback(array($this, 'queryColumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryColumnMock')));

        $request->method('getOptions')->willReturn(array('username' => 'testuser',
            'key' => $keys));

        $this->authorization->setRequest($request);

        $resp = $this->authorization->getAuthorizations();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

    }

    public function testGetAuthorizationsForUsedKeys() {

        $this->resourceParamMap = array(
            'application' => 'Test App',
            'module' => 'Test App Module',
        );

        $this->currentQueryMockResult['selectApplication'] = 1;
        $this->currentQueryMockResult['selectModule'] = 10;
        $this->currentQueryMockResult['selectEntityAP'] = 20;
        $this->currentQueryMockResult['selectDisabled'] = 0;
        $this->currentQueryMockResult['selectAuthorization'] = 1;

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->method('getResourceParam')
            ->with($this->callback(array($this, 'resourceParmsWith')))
            ->will($this->returnCallback(array($this, 'resourceParamsMock')));

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryColumnWithSql')),
                $this->callback(array($this, 'queryColumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryColumnMock')));

        $this->dbh->method('queryall_list')
            ->with($this->anything(), $this->equalTo('Test App'), $this->equalTo('Test App Module'))
            ->willReturn(['key1']);

        $request->method('getOptions')->willReturn(array('username' => 'testuser'));

        $this->authorization->setRequest($request);

        $resp = $this->authorization->getAuthorizations();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        $this->assertEquals([
            [
                'applicationName' => 'Test App',
                'module' => 'Test App Module',
                'key' => 'key1',
                'message' => '',
                'allowed' => 'true',
            ]
        ], $payload);

    }

    public function testGetAuthorizationsMissingUsername() {

        $this->resourceParamMap = array(
            'application' => 'Test App',
            'module' => 'Test App Module',
        );

        $keys = array('Test App Key 1', 'Test App Key 2');

        $this->currentQueryMockResult['selectApplication'] = 1;
        $this->currentQueryMockResult['selectModule'] = 10;
        $this->currentQueryMockResult['selectEntityAP'] = 20;
        $this->currentQueryMockResult['selectDisabled'] = 0;
        $this->currentQueryMockResult['selectAuthorization'] = 1;

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->method('getResourceParam')
            ->with($this->callback(array($this, 'resourceParmsWith')))
            ->will($this->returnCallback(array($this, 'resourceParamsMock')));

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryColumnWithSql')),
                $this->callback(array($this, 'queryColumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryColumnMock')));

        $request->method('getOptions')->willReturn(array('key' => $keys));

        $this->authorization->setRequest($request);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('No username found in request');

        $resp = $this->authorization->getAuthorizations();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

    }

    public function testIsAuthorized() {

        $this->resourceParamMap = array(
            'application' => 'Test App',
            'module' => 'Test App Module',
            'key' => 'Test App Key',
        );

        $this->currentQueryMockResult['selectApplication'] = 1;
        $this->currentQueryMockResult['selectModule'] = 10;
        $this->currentQueryMockResult['selectEntityAP'] = 20;
        $this->currentQueryMockResult['selectDisabled'] = 0;
        $this->currentQueryMockResult['selectAuthorization'] = 1;

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->method('getResourceParam')
            ->with($this->callback(array($this, 'resourceParmsWith')))
            ->will($this->returnCallback(array($this, 'resourceParamsMock')));

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryColumnWithSql')),
                $this->callback(array($this, 'queryColumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryColumnMock')));

        $request->method('getOptions')->willReturn(array('username' => 'testuser'));

        $this->authorization->setRequest($request);

        $resp = $this->authorization->isAuthorized();

        $payload = $resp->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

    }

    public function resourceParmsWith($subject) {
        $this->resourceParamName = $subject;
        return true;
    }

    public function resourceParamsMock() {
        return $this->resourceParamMap[$this->resourceParamName];
    }

    public function queryColumnWithSql($subject) {
        $this->currentQuery = '';

        if (strpos($subject, 'select application_id') !== false) {
            $this->currentQuery = 'selectApplication';
        }

        if (strpos($subject, 'select module_id') !== false) {
            $this->currentQuery = 'selectModule';
        }

        if (strpos($subject, 'select entity_id') !== false) {
            $this->currentQuery = 'selectEntity';
        }

        if (strpos($subject, 'select disabled') !== false) {
            $this->currentQuery = 'selectDisabled';
        }

        if (strpos($subject, 'select count(*)') !== false && strpos($subject, 'from a_authorization') !== false) {
            $this->currentQuery = 'selectAuthorization';
        }

        return true;
    }

    public function queryColumnWithParams($subject) {
        $this->queryColumnWithParams = $subject;

        if ($this->currentQuery === 'selectEntity') {
            if ($subject[1] === 'AP') {
                $this->currentQuery = 'selectEntityAP';
            } elseif ($subject[1] === 'LP') {
                $this->currentQuery = 'selectEntityLP';
            }
        }

        return true;
    }

    public function queryColumnMock() {
        if (isset($this->currentQuery) && isset($this->currentQueryMockResult[$this->currentQuery])) {
            return $this->currentQueryMockResult[$this->currentQuery];
        }

        return null;
    }

}
