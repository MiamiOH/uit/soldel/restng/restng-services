<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/18
 * Time: 8:00 AM
 */

namespace MiamiOH\RESTng\Service\CredentialValidator;


use MiamiOH\RESTng\App;

class MiamiCredentials implements CredentialValidatorInterface
{
    /** @var App */
    private $app;

    public function setApp(App $app): void
    {
        $this->app = $app;
    }

    public function validateToken(string $token): array
    {
        $response = $this->app->callResource('authentication.v1.validate', array(
            'params' => array('token' => $token)
        ));

        if ($response->getStatus() === App::API_OK) {
            $payload = $response->getPayload();

            return [
                'valid' => true,
                'username' => $payload['username'],
                'token' => $token,
            ];
        }

        return [
            'valid' => false,
            'username' => null,
            'token' => null,
        ];
    }

    public function validateUsernamePassword(string $username, string $password): array
    {
        $response = $this->app->callResource('authentication.v1.create', array(
            'data' => array(
                'username' => $username,
                'password' => $password,
                'type' => 'usernamePassword'
            )
        ));

        if (in_array($response->getStatus(), [App::API_CREATED, App::API_OK])) {
            $payload = $response->getPayload();

            return [
                'valid' => true,
                'username' => $payload['username'],
                'token' => $payload['token'],
            ];
        }

        return [
            'valid' => false,
            'username' => null,
            'token' => null,
        ];
    }
}