<?php

namespace MiamiOH\RESTng\Service\Config;

# Include library items from Marmot that we need.  We don't want all the overhead
# of Marmot, so there is not automatic inclusion.
#include_once('configMgr/configManager.php');

class Config extends \MiamiOH\RESTng\Service {

  private $database = '';
  private $dbh = '';

  public function setDatabase ($database) {
    $this->database = $database;

    $this->dbh = $this->database->getHandle('authman');
    $this->dbh->mu_trigger_error = false;
  }

  public function getConfigForApplication () {
    $request = $this->getRequest();
    $response = $this->getResponse();

    $application = $request->getResourceParam('application');
    $options = $request->getOptions();

    $user = $this->getApiUser();

    $authorized = $user->isAuthorized('CM-' . $application, $options['category'], 'view');

    if (!$authorized) {
      $authorized = $user->isAuthorized('CM-' . $application, $options['category'], 'add');
    }

    if (!$authorized) {
      $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
      return $response;
    }

    $format = $options['format'] ?? '';

    $configs = array();

    $this->log->debug('Get config for "' . $application . '" (' . $options['category'] . ')');
    if ($format === 'list') {
      $configs = $this->dbh->queryall_list('
                SELECT config_key, config_value
                FROM cm_config
                WHERE config_application = ?
                  and config_category = ?

                ', $application, $options['category']);
    } else {
      $configs = $this->dbh->queryall_assoc('
                SELECT config_key
                     ,config_application
                     ,config_key
                     ,config_data_type
                     ,config_data_structure
                     ,config_category
                     ,config_desc
                     ,config_value
                     ,activity_date
                     ,activity_user
                FROM cm_config
                WHERE config_application = ?
                  and config_category = ?
              ', $application, $options['category']);
    }

    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
    $response->setPayload($configs);

    return $response;
  }

}
