<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/18
 * Time: 8:01 AM
 */

namespace MiamiOH\RESTng\Service\AuthorizationValidator;


use MiamiOH\RESTng\App;

class AuthorizationManager implements AuthorizationValidatorInterface
{

    /** @var App */
    private $app;

    public function setApp(App $app): void
    {
        $this->app = $app;
    }

    public function validateAuthorizationForKey(AuthorizationSpecification $authSpec, string $username): bool
    {
        $authResponse = $this->app->callResource('authorization.v1.check', array(
            'params' => array(
                'application' => $authSpec->application(),
                'module' => $authSpec->module(),
                'key' => $authSpec->key(),
            ),
            'options' => array('username' => $username),
        ));

        return $authResponse->getStatus() === App::API_OK;
    }
}