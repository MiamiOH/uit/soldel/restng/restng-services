<?php

namespace MiamiOH\RESTng\Service\Random;

class Str extends \MiamiOH\RESTng\Service
{

    public function getString()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        $minimumLength = 20;
        $maximumLength = 50;
        $allowedCharacterTypes = array('uppercase', 'lowercase', 'numbers');

        if (isset($options['minimumLength']) && $options['minimumLength']) {
            if (is_numeric($options['minimumLength'])) {
                $minimumLength = $options['minimumLength'];
            } else {
                throw new \Exception('minimumLength must be numeric');
            }
        }

        if (isset($options['maximumLength']) && $options['maximumLength']) {
            if (is_numeric($options['maximumLength'])) {
                $maximumLength = $options['maximumLength'];
            } else {
                throw new \Exception('maximumLength must be numeric');
            }
        }

        if ($minimumLength > $maximumLength) {
            throw new \Exception('minimumLength cannot be greater than maximumLength');
        }

        if ($minimumLength < 1) {
            throw new \Exception('minimumLength must be greater than zero');
        }

        if ($maximumLength > 1000) {
            throw new \Exception('maximumLength cannot be greater than 1000');
        }

        $characterTypes = array();
        if (isset($options['allowedCharacters']) && is_array($options['allowedCharacters'])) {
            foreach ($options['allowedCharacters'] as $characterType) {
                if (!in_array($characterType, $allowedCharacterTypes)) {
                    throw new \Exception($characterType . ' is an invalid value for allowedCharacters');
                }

                $characterTypes[] = $characterType;
            }
        } else {
            $characterTypes = $allowedCharacterTypes;
        }

        if (count($characterTypes) === 0) {
            throw new \Exception('no character types specified');
        }

        $characterChoices = '';
        if (in_array('uppercase', $characterTypes)) {
            $characterChoices .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        if (in_array('lowercase', $characterTypes)) {
            $characterChoices .= 'abcdefghijklmnopqrstuvwxyz';
        }
        if (in_array('numbers', $characterTypes)) {
            $characterChoices .= '1234567890';
        }

        $characterChoicesCount = strlen($characterChoices);

        if ($minimumLength != $maximumLength) {
            $stringLength = mt_rand($minimumLength, $maximumLength);
        } else {
            $stringLength = $minimumLength;
        }

        $value = '';
        for ($i = 0; $i < $stringLength; $i++) {
            $value .= substr($characterChoices, mt_rand(0, $characterChoicesCount - 1), 1);
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload(array('string' => $value));

        return $response;
    }

}
