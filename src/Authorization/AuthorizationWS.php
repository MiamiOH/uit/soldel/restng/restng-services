<?php
namespace MiamiOH\RESTng\Service\Authorization;
class AuthorizationWS extends \MiamiOH\RESTng\Service
{
    private $error = '';
    private $restService;
    private $dnFormat = 'uid=%s,ou=people,dc=muohio,dc=edu';

    public function setRestService($client)
    {
        $this->restService = $client;
    }

    public function getRestService()
    {
        return $this->restService;
    }

    public function getAuthorization()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $application = $request->getResourceParam('application');
        $module = $request->getResourceParam('module');
        $key = $request->getResourceParam('key');

        $options = $request->getOptions();
        $username = $options['username'] ? $options['username'] : '';

        if (!$username) {
            throw new \Exception('No username found in request');
        }

        $rc = $this->checkAuthorization($username, $application, $module, $key);

        if ($rc === false) {
            $dn = sprintf($this->dnFormat, $username);
            $rc = $this->checkAuthorization($dn, $application, $module, $key);
        }

        $authorization = array(
            // 'authorizationResource' => $resourceURL,
            'applicationName' => $application,
            'module' => $module,
            'key' => $key,
            'message' => $this->error,
            'allowed' => ($rc === TRUE ? 'true' : 'false'),
        );

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($authorization);

        return $response;
    }

    public function getAuthorizations()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $application = $request->getResourceParam('application');
        $module = $request->getResourceParam('module');
        $authorizations = array();

        $options = $request->getOptions();
        $username = $options['username'] ? $options['username'] : '';

        if (!$username) {
            throw new \Exception('No username found in request');
        }

        if (empty($options['key'])) {
            throw new \Exception('Missing key(s) to check');
        }

        foreach ($options['key'] as $key) {
            $rc = $this->checkAuthorization($username, $application, $module, $key);
            if ($rc === false) {
                $dn = sprintf($this->dnFormat, $username);
                $rc = $this->checkAuthorization($dn, $application, $module, $key);
                # if we got back a success, we now know the official format - remember it for next time through the loop
                if ($rc === TRUE) {
                    $username = $dn;
                }
            }

            $authorization = array(
                // 'authorizationResource' => $resourceURL,
                'applicationName' => $application,
                'module' => $module,
                'key' => $key,
                'message' => $this->error,
                'allowed' => ($rc === TRUE ? 'true' : 'false'),
            );

            $authorizations[] = $authorization;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($authorizations);

        return $response;
    }

    public function isAuthorized()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $application = $request->getResourceParam('application');
        $module = $request->getResourceParam('module');
        $key = $request->getResourceParam('key');

        $options = $request->getOptions();
        $username = $options['username'] ? $options['username'] : '';

        if (!$username) {
            throw new \Exception('No username found in request');
        }

        $rc = $this->checkAuthorization($username, $application, $module, $key);

        if ($rc === false) {
            $dn = sprintf($this->dnFormat, $username);
            $rc = $this->checkAuthorization($dn, $application, $module, $key);
        }

        $isAuthorized = $rc ? true : false;
        $authorized = array('authorized' => $isAuthorized);

        $response->setStatus($isAuthorized ? \MiamiOH\RESTng\App::API_OK : \MiamiOH\RESTng\App::API_UNAUTHORIZED);
        $response->setPayload($authorized);

        return $response;
    }

    public function checkAuthorization($user, $application, $module, $key)
    {
        if (!$application) {
            throw new \Exception('No application name given');
        }
        if (!$module) {
            throw new \Exception('No module name given');
        }
        if (!$key) {
            throw new \Exception('No grant key given');
        }

        /** @var \GuzzleHttp\Client $client */
        $client = $this->restService->newClient(array('base_uri' => env('MU_API_URL', 'https://example.com')));
        $getUrl = '/api/authorization/v1/' . urlencode($application) .
            '/' . urlencode($module) . '/' . urlencode($key) .
            "?username=" . urlencode($user);

        $authResponse = json_decode($client->get($getUrl)->getBody(), true);

        return (string)$authResponse['data']['allowed'] === 'true' ? true : false;
    }
}