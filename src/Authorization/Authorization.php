<?php

namespace MiamiOH\RESTng\Service\Authorization;

use MiamiOH\RESTng\Service\Apm\Transaction;
use MiamiOH\RESTng\Util\CacheKey;
use MiamiOH\RESTng\Util\CacheKeyNull;
use MiamiOH\RESTng\Util\CacheKeyString;
use function Symfony\Component\String\u;

class Authorization extends \MiamiOH\RESTng\Service
{

    private $error = '';

    private $dnFormat = 'uid=%s,ou=people,dc=muohio,dc=edu';

    private $database = '';
    private $dbh = '';
    private $ldap = '';

    /** @var  \MiamiOH\RESTng\Connector\DataSourceFactory $dsFactory */
    private $dsFactory;

    private $resolvedAdEntries = [];

    public function setDatabase($database)
    {
        $this->database = $database;

        $this->dbh = $this->database->getHandle('authman');
        $this->dbh->mu_trigger_error = false;
    }

    public function setLdap($ldap)
    {
        $this->ldap = $ldap;
    }

    public function setDataSourceFactory($dsFactory)
    {
        $this->dsFactory = $dsFactory;
    }

    public function setApmTransaction(Transaction $transaction): void
    {
        $this->transaction = $transaction;
    }

    public function getAuthorizationCacheKey(): CacheKey
    {
        $request = $this->getRequest();

        $application = $request->getResourceParam('application');
        $module = $request->getResourceParam('module');
        $key = $request->getResourceParam('key');

        $options = $request->getOptions();

        if (empty($options['username'])) {
            return new CacheKeyNull();
        }

        $username = $options['username'];

        return new CacheKeyString(
            'authorization:{username}:{application}:{module}:{key}',
            [
                'username' => $username,
                'application' => $application,
                'module' => $module,
                'key' => $key,
            ]
        );
    }

    function getAuthorization()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $application = $request->getResourceParam('application');
        $module = $request->getResourceParam('module');
        $key = $request->getResourceParam('key');

        $options = $request->getOptions();

        $username = $options['username'] ? $options['username'] : '';

        if (!$username) {
            throw new \Exception('No username found in request');
        }

        $rc = $this->checkAuthorization($username, $application, $module, $key);

        if ($rc === false) {
            $dn = sprintf($this->dnFormat, $username);

            $rc = $this->checkAuthorization($dn, $application, $module, $key);

            # if we got back a success, we now know the official format - remember it for next time through the loop
            if ($rc === TRUE) {
                $username = $dn;
            }
        }

        if ($this->error) {
            throw new \Exception($this->error);
        }

        // $resourceURL = $app->urlFor('authorization', array('application' => urlencode($application),
        //     'module' => urlencode($module), 'key' => urlencode($keyList)));

        $authorization = array(
            // 'authorizationResource' => $resourceURL,
            'applicationName' => $application,
            'module' => $module,
            'key' => $key,
            'message' => '',
            'allowed' => ($rc === TRUE ? 'true' : 'false'),
        );

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($authorization);

        return $response;
    }

    public function getAuthorizationsCacheKey(): CacheKey
    {
        $request = $this->getRequest();

        $application = $request->getResourceParam('application');
        $module = $request->getResourceParam('module');

        $options = $request->getOptions();

        if (empty($options['username'])) {
            return new CacheKeyNull();
        }

        $keys = $options['key'] ?? [];

        $username = $options['username'];

        return new CacheKeyString(
            'authorization:list:{username}:{application}:{module}:{keyList}',
            [
                'username' => $username,
                'application' => $application,
                'module' => $module,
                'keyList' => implode(',', $keys),
            ]
        );
    }

    function getAuthorizations()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $application = $request->getResourceParam('application');
        $module = $request->getResourceParam('module');

        $authorizations = array();

        $options = $request->getOptions();

        $username = isset($options['username']) ? $options['username'] : '';

        if (!$username) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('No username found in request');
        }

        $keys = $options['key'] ?? $this->getUsedKeys($application, $module);

        foreach ($keys as $key) {
            $rc = $this->checkAuthorization($username, $application, $module, $key);

            if ($rc === false) {
                $dn = sprintf($this->dnFormat, $username);

                $rc = $this->checkAuthorization($dn, $application, $module, $key);

                # if we got back a success, we now know the official format - remember it for next time through the loop
                if ($rc === TRUE) {
                    $username = $dn;
                }
            }

            if ($this->error) {
                throw new \Exception($this->error);
            }

            // $resourceURL = $app->urlFor('authorization', array('application' => urlencode($application),
            //     'module' => urlencode($module), 'key' => urlencode($keyList)));

            $authorization = array(
                // 'authorizationResource' => $resourceURL,
                'applicationName' => $application,
                'module' => $module,
                'key' => $key,
                'message' => '',
                'allowed' => ($rc === TRUE ? 'true' : 'false'),
            );

            $authorizations[] = $authorization;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($authorizations);

        return $response;
    }

    function isAuthorized()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $application = $request->getResourceParam('application');
        $module = $request->getResourceParam('module');
        $key = $request->getResourceParam('key');

        $options = $request->getOptions();

        $username = $options['username'] ? $options['username'] : '';

        if (!$username) {
            throw new \Exception('No username found in request');
        }

        $span = $this->transaction->startSpan('Checking authorization for '. $username);
        $rc = $this->checkAuthorization($username, $application, $module, $key);
        $span->stop();

        if ($rc === false) {
            $dn = sprintf($this->dnFormat, $username);

            $span = $this->transaction->startSpan('Checking authorization for ' . $dn);
            $rc = $this->checkAuthorization($dn, $application, $module, $key);
            $span->stop();
        }

        if ($this->error) {
            throw new \Exception($this->error);
        }

        $isAuthorized = $rc ? true : false;

        $authorized = array('authorized' => $isAuthorized);
        $response->setStatus($isAuthorized ? \MiamiOH\RESTng\App::API_OK : \MiamiOH\RESTng\App::API_UNAUTHORIZED);

        $response->setPayload($authorized);

        return $response;
    }

    public function checkAuthorization($user, $application, $module, $key)
    {
        if (!$application) {
            $this->log->warn('No application name given for authorization check');
            return false;
        }

        if (!$module) {
            $this->log->warn('No module name given for authorization check');
            return false;
        }

        if (!$key) {
            $this->log->warn('No grant key name given for authorization check');
            return false;
        }

        $applicationId = $this->dbh->queryfirstcolumn('
            select application_id
                    from a_application
                    where lower(name) = lower(?)
            ', $application);

        if ($applicationId === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET || !$applicationId) {
            $this->log->warn('Application name not found: ' . $application);
            return false;
        }

        $moduleId = $this->dbh->queryfirstcolumn('
            select module_id
                from a_module
                where lower(name) = lower(?)
                      and application_id = ?
            ', array($module, $applicationId));

        if ($moduleId === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET || !$moduleId) {
            $this->log->warn('Module name not found: ' . $module);
            return false;
        }

        // Try to get the entity id first as an application person.
        $entityId = $this->dbh->queryfirstcolumn('
            select entity_id
                from a_entity
                where lower(dn) = lower(?)
                      and type = ?
                          and (scope = ?
                                or (scope = ?
                                    and application_id = ?))
            ', array($user, 'AP', 'S', 'A', $applicationId));

        // If not an AP, try as an LDAP person
        if ($entityId === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $entityId = $this->dbh->queryfirstcolumn('
                select entity_id
                    from a_entity
                    where lower(dn) = lower(?)
                          and type = ?
                              and (scope = ?
                                    or (scope = ?
                                        and application_id = ?))
                ', array($user, 'LP', 'S', 'A', $applicationId));
        }


        if ($entityId === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $entityId = null;
        } else {
            $disabled = $this->dbh->queryfirstcolumn('
                select disabled
                    from a_entity
                    where entity_id = ?
                ', $entityId);

            if ($disabled) {
                $this->log->warn('User "' . $user . '" is disabled and cannot be authorized');
                return false;
            }
        }

        // Now we have the application, module and entity. Check authorizations.

        // First check explicitly granted application level auths.
        $authorized = $this->dbh->queryfirstcolumn('
            select count(*)
                from a_authorization, a_grant_key
                where a_authorization.module_id = ?
                  and a_authorization.status = ?
                  and a_authorization.grant_key_id = a_grant_key.grant_key_id
                  and upper(a_grant_key.grant_key) = upper(?)
                  and ( a_authorization.entity_id = ?
                        or a_authorization.entity_id in ( select unique group_id
                                                            from a_group_membership
                                                            start with member_id = ?
                                                            connect by prior group_id = member_id ) )
            ', array($moduleId, 'A', $key, $entityId, $entityId));

        // If no auths, then check for auths granted to ldap groups and see if the user is a member
        $hasADConnection = false;

        try {
            $this->dsFactory->getDataSource('RTFU_AD');
            $hasADConnection = true;
        } catch (\Exception $e) {
            $hasADConnection = false;
        }

        if ($hasADConnection && $authorized == 0) {
            $authGroups = $this->dbh->queryall_list('
                select a_entity.dn
                  from a_authorization, a_entity, a_grant_key
                 where a_authorization.entity_id = a_entity.entity_id
                       and a_entity.type = ?
                       and a_authorization.module_id = ?
                       and a_authorization.status = ?
                       and a_authorization.grant_key_id = a_grant_key.grant_key_id
                       and upper(a_grant_key.grant_key) = upper(?)
                ', 'LG', $moduleId, 'A', $key);

            $adEntry = $this->resolveAdEntry($user, array('memberof', 'cn'));

            $memberOf = array();
            if (!empty($adEntry['memberof']) && is_array($adEntry['memberof'])) {
                $memberOf = array_map('strtolower', $adEntry['memberof']);
            }

            // Check any LDAP groups directly granted
            foreach ($authGroups as $authGroup) {
                if (in_array(strtolower($authGroup), $memberOf)) {
                    $authorized = 1;
                    break;
                }
            }

            // If there are still no auths, check for any LDAP groups that
            // are members of application groups which are granted the key
            if ($authorized == 0) {
                $appGroupIds = $this->dbh->queryall_list('
                    select a_entity.entity_id
                      from a_authorization, a_entity, a_grant_key
                     where a_authorization.entity_id = a_entity.entity_id
                           and a_entity.type = ?
                           and a_authorization.module_id = ?
                           and a_authorization.status = ?
                           and a_authorization.grant_key_id = a_grant_key.grant_key_id
                           and upper(a_grant_key.grant_key) = upper(?)
                    ', 'AG', $moduleId, 'A', $key);

                foreach ($appGroupIds as $groupId) {
                    $ldapGroups = $this->dbh->queryall_list('
                        select a_entity.dn
                          from a_entity
                         where a_entity.type = ?
                               and a_entity.entity_id in (select unique member_id
                                                            from a_group_membership
                                                            start with group_id = ?
                                                            connect by prior member_id = group_id)
                        ', 'LG', $groupId);

                    foreach ($ldapGroups as $authGroup) {
                        if (in_array(strtolower($authGroup), $memberOf)) {
                            $authorized = 1;
                            break;
                        }
                    }

                    if ($authorized) {
                        break;
                    }
                }
            }
        }

        return $authorized > 0;

    }

    private function resolveAdEntry($user = '', $attributes = array())
    {
        if (array_key_exists($user, $this->resolvedAdEntries)) {
            return $this->resolvedAdEntries[$user];
        }

        $this->resolvedAdEntries[$user] = $this->getADEntry($user, $attributes);

        return $this->resolvedAdEntries[$user];
    }

    private function getUsedKeys(string $application, string $module): array
    {
        return $this->dbh->queryall_list('
            select distinct a_grant_key.grant_key as key
            from a_authorization
                inner join a_module on a_authorization.module_id = a_module.module_id
                inner join a_application on a_module.application_id = a_application.application_id
                inner join a_grant_key on a_authorization.grant_key_id = a_grant_key.grant_key_id
            where a_application.name = ?
                and a_module.name = ?
                        ', $application, $module);
    }

    private function getADEntry($user = '', $attributes = array())
    {
        if (!$user) {
            $user = $this->user;
        }

        if (!$user) {
            return null;
        }

        // See if user is given as a dn.
        if (strstr($user, ',') !== false) {
            $dnParts = explode(',', $user);
            [$pre, $cn] = explode('=', $dnParts[0]);
            $user = $cn;
        }

        $span = $this->transaction->startSpan('AD Connect');
        $span->setType('external');
        $span->setSubType('ldap');

        $ad = $this->ldap->getHandle('RTFU_AD');

        $span->stop();

        $entry = array();

        if ($ad->ds) {
            $base = 'ou=people,dc=it,dc=muohio,dc=edu';

            $span = $this->transaction->startSpan(sprintf('AD Search cn=%s', $user));
            $span->setType('external');
            $span->setSubType('ldap');

            $sr = ldap_search($ad->ds, $base, 'cn=' . $user,
                $attributes, 0, 0, 30);

            if ($sr === false) {
                return null;
            }

            $srInfo = ldap_get_entries($ad->ds, $sr);

            if ($srInfo['count'] == 1) {
                $adEntry = ldap_first_entry($ad->ds, $sr);
                $entry['dn'] = ldap_get_dn($ad->ds, $adEntry);

                $attribute = strtolower(ldap_first_attribute($ad->ds, $adEntry));
                while ($attribute) {
                    $value = ldap_get_values($ad->ds, $adEntry, $attribute);
                    unset($value['count']);

                    // Always return arrays.
                    $entry[$attribute] = $value;

                    $attribute = strtolower(ldap_next_attribute($ad->ds, $adEntry));
                }
            }

            $span->stop();
        }

        return $entry;


    }
}
