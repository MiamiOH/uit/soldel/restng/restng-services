<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 9:34 PM
 */

namespace MiamiOH\RESTng\Service\Resources;


class RandomResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'Random',
            'description' => 'Resources for providing random values'
        ));

        $this->addDefinition(array(
            'name' => 'Random.String',
            'type' => 'object',
            'properties' => array(
                'string' => array(
                    'type' => 'string',
                ),
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'RandomString',
            'class' => 'MiamiOH\RESTng\Service\Random\Str',
            'description' => 'Service to provide random string functions.',
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'random.string',
            'description' => 'Returns a random string.',
            'tags' => array('Random'),
            'pattern' => '/random/string',
            'service' => 'RandomString',
            'method' => 'getString',
            'middleware' => array(),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A random string',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Random.String',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'random.string.authn',
            'description' => 'Returns a random string. Requires authentication',
            'tags' => array('Random'),
            'pattern' => '/random/string/authn',
            'service' => 'RandomString',
            'method' => 'getString',
            'middleware' => array('authenticate' => array('type' => 'token')),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A random string',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Random.String',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'random.string.authn.anon',
            'description' => 'Returns a random string. Supports anonymous or authenticated access',
            'tags' => array('Random'),
            'pattern' => '/random/string/authn/anon',
            'service' => 'RandomString',
            'method' => 'getString',
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A random string',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Random.String',
                    )
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}