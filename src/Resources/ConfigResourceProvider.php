<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 9:34 PM
 */

namespace MiamiOH\RESTng\Service\Resources;


class ConfigResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'Configuration',
            'description' => 'Resources for getting configuration values'
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'ConfigMgr',
            'class' => 'MiamiOH\RESTng\Service\Config\Config',
            'description' => 'Provides access to ConfigMgr data.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'config.v1',
            'description' => 'Returns a list of ConfigMgr keys for the given category.',
            'tags' => array('Configuration'),
            'pattern' => '/config/v1/:application',
            'service' => 'ConfigMgr',
            'method' => 'getConfigForApplication',
            'params' => array( 'application' => array('description' => 'Application name')),
            'options' => array(
                'format' => array('description' => 'One of "complete" or "list"'),
                'category' => array('required' => true, 'description' => 'Category name'),
            ),
            'middleware' => array('authenticate' => array('type' => 'token')),
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}
