<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 9:34 PM
 */

namespace MiamiOH\RESTng\Service\Resources;


class AuthenticationResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'Authentication',
            'description' => 'Resources for authenticating users'
        ));

        $this->addDefinition(array(
            'name' => 'Authentication.Validation',
            'type' => 'object',
            'properties' => array(
                'token' => array(
                    'type' => 'string',
                ),
                'username' => array(
                    'type' => 'string',
                ),
                'credential_source' => array(
                    'type' => 'string',
                    'enum' => ['local', 'ldap'],
                ),
                'expiration_time' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Authentication.Token',
            'type' => 'object',
            'properties' => array(
                'token' => array(
                    'type' => 'string',
                ),
                'username' => array(
                    'type' => 'string',
                ),
                'credentialSource' => array(
                    'type' => 'string',
                    'enum' => ['local', 'ldap'],
                ),
                'tokenLifetime' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Authentication.Credential',
            'type' => 'object',
            'properties' => array(
                'username' => array(
                    'type' => 'string',
                ),
                'password' => array(
                    'type' => 'string',
                ),
                'type' => array(
                    'type' => 'string',
                    'enum' => ['usernamePassword'],
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Authentication.User',
            'type' => 'object',
            'properties' => array(
                'username' => array(
                    'type' => 'string',
                ),
                'password' => array(
                    'type' => 'string',
                ),
                'createDate' => array(
                    'type' => 'string',
                ),
                'creatorId' => array(
                    'type' => 'string',
                ),
                'expirationDate' => array(
                    'type' => 'string',
                ),
                'comments' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Authentication.User.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Authentication.User'
            )
        ));
    }

    public function registerServices(): void
    {

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'authentication.v1.validate',
            'summary' => 'Validates the provided token',
            'description' => 'Validates the given token. Returns the username and token information if valid. ' .
                'Returns a 404 if the token is not valid.',
            'tags' => array('Authentication'),
            'pattern' => '/authentication/v1/:token',
            'service' => 'Authentication',
            'method' => 'validateToken',
            'params' => array(
                'token' => array('description' => 'The token value to look up'),
            ),
            'middleware' => array(),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A validated token response',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Authentication.Validation',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Token not found'
                )
            )
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'authentication.v1.create',
            'summary' => 'Username/password authentication',
            'description' => 'Authenticates the given username/password and returns a token on success. Requires userName and password in the data. '
                . 'Also accepts applicationName',
            'tags' => array('Authentication'),
            'pattern' => '/authentication/v1',
            'body' => array(
                'description' => 'A credential object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Authentication.Credential'
                )
            ),
            'service' => 'Authentication',
            'method' => 'authenticateUser',
            'middleware' => array(),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'The newly created token',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Authentication.Token',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'authentication.v2.user.all',
            'summary' => 'Get all local users.',
            'description' => 'Get a collection of all local users.',
            'tags' => array('Authentication'),
            'pattern' => '/authentication/v2/user',
            'service' => 'Authentication',
            'method' => 'getUsers',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Authentication',
                        'key' => 'admin'
                    ),
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A collection of users',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Authentication.User.Collection',
                    )
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'authentication.v2.user.read',
            'summary' => 'Get data about the specific user.',
            'description' => 'Get data about the specific user.',
            'tags' => array('Authentication'),
            'pattern' => '/authentication/v2/user/:username',
            'service' => 'Authentication',
            'method' => 'getUser',
            'params' => array(
                'username' => array('description' => 'The username to look up'),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Authentication',
                        'key' => 'admin'
                    ),
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A user response',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Authentication.User',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Username not found'
                )
            )
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'authentication.v2.user.create',
            'summary' => 'Create a new user.',
            'description' => 'Create a new user.',
            'tags' => array('Authentication'),
            'pattern' => '/authentication/v2/user',
            'service' => 'Authentication',
            'method' => 'createUser',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Authentication',
                        'key' => 'admin'
                    ),
                ),
            ),
            'body' => array(
                'description' => 'A user model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Authentication.User'
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'The newly created user object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Authentication.User',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'authentication.v2.user.update',
            'summary' => 'Update an existing user.',
            'description' => 'Update an existing user.',
            'tags' => array('Authentication'),
            'pattern' => '/authentication/v2/user/:username',
            'service' => 'Authentication',
            'method' => 'updateUser',
            'params' => array(
                'username' => array('description' => 'The username to update'),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Authentication',
                        'key' => 'admin'
                    ),
                ),
            ),
            'body' => array(
                'description' => 'A user model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Authentication.User'
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'The user was successfully updated',
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'authentication.v2.user.update_password',
            'summary' => 'Update the password of an existing user.',
            'description' => 'Update the password of an existing user.',
            'tags' => array('Authentication'),
            'pattern' => '/authentication/v2/user/:username/password',
            'service' => 'Authentication',
            'method' => 'updateUserPassword',
            'params' => array(
                'username' => array('description' => 'The username to update'),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Authentication',
                        'key' => 'admin'
                    ),
                ),
            ),
            'body' => array(
                'description' => 'A user model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Authentication.User'
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'The password was successfully updated',
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'authentication.v2.user.delete',
            'summary' => 'Delete the given user.',
            'description' => 'Delete the given user.',
            'tags' => array('Authentication'),
            'pattern' => '/authentication/v2/user/:username',
            'service' => 'Authentication',
            'method' => 'deleteUser',
            'params' => array(
                'username' => array('description' => 'The username to look up'),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Authentication',
                        'key' => 'admin'
                    ),
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'The user was deleted.',
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}