<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 9:34 PM
 */

namespace MiamiOH\RESTng\Service\Resources;


class AuthorizationResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'Authorization',
            'description' => 'Resources for authorizing access'
        ));

        $this->addDefinition(array(
            'name' => 'Authorization',
            'type' => 'object',
            'properties' => array(
                'applicationName' => array(
                    'type' => 'string',
                ),
                'module' => array(
                    'type' => 'string',
                ),
                'key' => array(
                    'type' => 'string',
                ),
                'message' => array(
                    'type' => 'string',
                ),
                'allowed' => array(
                    'type' => 'boolean',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Authorization.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Authorization'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Authorization.Check',
            'type' => 'object',
            'properties' => array(
                'authorized' => array(
                    'type' => 'boolean',
                ),
            )
        ));
    }

    public function registerServices(): void
    {

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'authorization.v1',
            'description' => 'Returns authorization information for the given application, module and key.',
            'tags' => array('Authorization'),
            'pattern' => '/authorization/v1/:application/:module/:key',
            'service' => 'Authorization',
            'method' => 'getAuthorization',
            'params' => array(
                'application' => array('description' => 'Application name'),
                'module' => array('description' => 'Module name'),
                'key' => array('description' => 'Grant key')
            ),
            'options' => array(
                'username' => array('type' => 'single', 'required' => true, 'description' => 'Username to check'),
            ),
            'middleware' => array(),
            'cache' => [
                'enabled' => true,
                'ttl' => env('CACHE_TTL', 300),
                'keyMethod' => 'getAuthorizationCacheKey',
                'cacheResponses' => [
                    \MiamiOH\RESTng\App::API_OK,
                ],
            ],
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A authorization object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Authorization',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'authorization.v1.check',
            'description' => 'Returns true or false for the authorized status of the user for the application, module and key.',
            'tags' => array('Authorization'),
            'pattern' => '/authorization/v1/check/:application/:module/:key',
            'service' => 'Authorization',
            'method' => 'isAuthorized',
            'params' => array(
                'application' => array('description' => 'Application name'),
                'module' => array('description' => 'Module name'),
                'key' => array('description' => 'Grant key')
            ),
            'options' => array(
                'username' => array('type' => 'single', 'description' => 'Username to check'),
            ),
            'middleware' => array(),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A authorization check object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Authorization.Check',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'authorization.v1.key',
            'description' => 'Returns a list of authorizations for the user from the provided list of keys.',
            'tags' => array('Authorization'),
            'pattern' => '/authorization/v1/:application/:module',
            'service' => 'Authorization',
            'method' => 'getAuthorizations',
            'params' => array(
                'application' => array('description' => 'Application name'),
                'module' => array('description' => 'Module name'),
            ),
            'options' => array(
                'username' => array('type' => 'single', 'description' => 'Username to check'),
                'key' => array('type' => 'list', 'description' => 'List of keys to check'),
            ),
            'middleware' => array(),
            'cache' => [
                'enabled' => true,
                'ttl' => env('CACHE_TTL', 300),
                'keyMethod' => 'getAuthorizationsCacheKey',
                'cacheResponses' => [
                    \MiamiOH\RESTng\App::API_OK,
                ],
            ],
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A collection of authorization objects',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Authorization.Collection',
                    )
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}
