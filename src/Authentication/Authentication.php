<?php

namespace MiamiOH\RESTng\Service\Authentication;

use Carbon\Carbon;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\Exceptions\AuthenticationUserMismatchException;
use MiamiOH\RESTng\Service\Exceptions\AuthenticationUserNotFoundException;
use MiamiOH\RESTng\Service\Exceptions\UnkownHashAlgorithmException;

class Authentication extends \MiamiOH\RESTng\Service
{
    public const AUTHENTICATION_USER_PASSWORD_HASH_MD5 = 1;

    private $database = '';
    private $dbh = '';
    private $ldapFactory = null;

    public function setDatabase($database)
    {
        $this->database = $database;

        $this->dbh = $this->database->getHandle('authman');
        $this->dbh->mu_trigger_error = false;
    }

    public function setLdapFactory($ldap) {
        $this->ldapFactory = $ldap;
    }

    public function authenticateUser()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getData();

        if (!(isset($data['type']) && $data['type'])) {
            throw new \Exception(__CLASS__ . '::authenticateUser() requires type');
        }

        if (!in_array($data['type'], array('usernamePassword', 'cas-proxy'))) {
            throw new \Exception(__CLASS__ . '::authenticateUser() type must be one of "usernamePassword" or "cas-proxy"');
        }

        $status = \MiamiOH\RESTng\App::API_OK;
        $respData = array();

        switch ($data['type']) {

            case 'usernamePassword':

                if (!(isset($data['username']) && $data['username'])) {
                    throw new \Exception(__CLASS__ . '::authenticateUser() requires username for type usernamePassword');
                }

                if (!(isset($data['password']) && $data['password'])) {
                    throw new \Exception(__CLASS__ . '::authenticateUser() requires password for type usernamePassword');
                }

                $credentialSource = 'local';
                $credentialsValid = $this->validateCredentialsLocalUser($data['username'], $data['password']);
                if(!$credentialsValid) {
                    $credentialsValid = $this->validateCredentialsLDAP($data['username'], $data['password']);
                    $credentialSource = 'LDAP';
                }

                if ($credentialsValid) {

                    $tokenLifetimeObject = date_create();
                    $tokenLifetimeObject->add(new \DateInterval('PT1H'));
                    $tokenLifetime = $tokenLifetimeObject->format('Y-m-d\TH:i:s');

                    $authResponse = array();
                    $randomStringResponse = $this->callResource('random.string')->getPayload();

                    $token = $randomStringResponse['string'];
                    $username = $data['username'];
                    $application = isset($data['applicationName']) ? $data['applicationName'] : '';
                    $authenticationLocation = $this->locationFor('authentication.v1.validate', array('token' => $token));

                    $this->saveToken($token, $username, $application, $credentialSource, $tokenLifetime);

                    $authResponse['token'] = $token;
                    $authResponse['authenticationLocation'] = $authenticationLocation;
                    $authResponse['username'] = $username;
                    $authResponse['credentialSource'] = $credentialSource;
                    $authResponse['tokenLifetime'] = $tokenLifetime;

                    $response->setPayload($authResponse);

                } else {
                    $status = \MiamiOH\RESTng\App::API_UNAUTHORIZED;
                }

                break;

        }

        $response->setStatus($status);
        if ($respData) {
            $response->setData($respData);
        }

        return $response;
    }

    public function validateToken()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $token = $request->getResourceParam('token');

        $query = '';
        if ($this->dbh->getType() == 'MySQL') {
            $query = '
        SELECT token, username, credential_source, date_format(expiration_time, \'%Y-%m-%d"T"%T\') as expiration_time
                   FROM ws_authentication_token
                   WHERE token = ?
                     AND expiration_time >= current_date
                ';
        } else {
            $query = '
        SELECT token, username, credential_source, to_char(expiration_time, \'YYYY-MM-DD"T"HH24:MI:SS\') as expiration_time
                   FROM ws_authentication_token
                   WHERE token = ?
                     AND expiration_time >= SYSDATE
                ';
        }
        $tokenInfo = $this->dbh->queryFirstRow_assoc($query, $token);

        if ($this->dbh->getErrorNum()) {
            throw new \Exception('Error querying token: ' . $this->dbh->getErrorNum() . ' - ' . $this->dbh->getErrorString());
        }

        if ($tokenInfo === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $tokenInfo = array();
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } else {
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($tokenInfo);
        }

        return $response;
    }

    private function validateCredentialsLocalUser($username, $password)
    {
        $query = '';

        if ($this->dbh->getType() == 'MySQL') {
            $query = '
        SELECT username,
                       algorithm,
                       credential_hash,
                       date_format(create_date, \'%Y-%m-%d\') as create_date,
                       create_username,
                       date_format(expiration_date, \'%Y-%m-%d\') as expiration_date,
                       comments
                  FROM ws_authentication_local_users
                 WHERE lower(username) = ?
                  AND expiration_date >= current_date
          ';
        } else {
            $query = '
        SELECT username,
                       algorithm,
                       credential_hash,
                       to_char(create_date, \'YYYY-MM-DD\') as create_date,
                       create_username,
                       to_char(expiration_date, \'YYYY-MM-DD\') as expiration_date,
                       comments
                  FROM ws_authentication_local_users
                 WHERE lower(username) = ?
                  AND expiration_date >= SYSDATE
          ';
        }

        $userInfo = $this->dbh->queryFirstRow_assoc($query, strtolower($username));

        if ($userInfo === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            return false;
        }

        switch ($userInfo['algorithm']) {
            case 1:
                return hash('md5', $password) === $userInfo['credential_hash'];
                break;
            default:
                throw new \Exception('Unkown local user hash algorithm: ' . $userInfo['algorithm']);
        }
    }

    public function saveToken($token, $username, $application, $credentialSource, $tokenLifetime)
    {
        $query = '';
        if ($this->dbh->getType() == 'MySQL') {
            $query = '
      INSERT INTO ws_authentication_token
               (token, username, application_name, credential_source, issued_time, expiration_time)
           VALUES (?, ?, ?, ?, current_date, str_to_date(?, \'%Y-%m-%d"T"%T\'))
        ';
        } else {
            $query = '
      INSERT INTO ws_authentication_token
               (token, username, application_name, credential_source, issued_time, expiration_time)
           VALUES (?, ?, ?, ?, sysdate, to_date(?, \'YYYY-MM-DD"T"HH24:MI:SS\'))
        ';
        }

        $result = $this->dbh->perform($query, $token, $username, $application, $credentialSource, $tokenLifetime);

        return $result;
    }

    private function validateCredentialsLDAP($username, $password)
    {
        $ldapHandler = $this->ldapFactory->getHandle('ldap_auth');
        return $ldapHandler->authenticate(sprintf('uid=%s' , $username), $password);
    }

    public function getUsers()
    {
        $response = $this->getResponse();

        $userList = $this->getUserRecords();

        $response->setPayload($userList);

        return $response;
    }

    private function getUserRecords(): array
    {
        $query = '
            select username, to_char(create_date, \'YYYY-MM-DD\') as create_date, create_username, 
                    to_char(expiration_date, \'YYYY-MM-DD\') as expiration_date, comments 
                from ws_authentication_local_users 
                order by username
            ';

        $records = $this->dbh->queryall_array($query);

        $models = [];

        foreach ($records as $record) {
            $models[] = $this->makeUserModel($record);
        }

        return $models;
    }

    private function makeUserModel(array $record): array
    {
        return [
            'username' => $record['username'],
            'password' => null,
            'creationDate' => Carbon::parse($record['create_date']),
            'creatorId' => strtolower($record['create_username']),
            'expirationDate' => Carbon::parse($record['expiration_date']),
            'comments' => $record['comments'],
        ];
    }

    public function getUser()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $username = $request->getResourceParam('username');

        try {
            $user = $this->getUserModel($username);

            $response->setPayload($user);
        } catch (AuthenticationUserNotFoundException $e) {
            $response->setStatus(App::API_NOTFOUND);
        }

        return $response;

    }

    /**
     * @param string $username
     * @return array
     * @throws AuthenticationUserNotFoundException
     */
    private function getUserModel(string $username): array
    {
        return $this->makeUserModel($this->getUserRecord($username));
    }

    /**
     * @param string $username
     * @return array
     * @throws AuthenticationUserNotFoundException
     */
    private function getUserRecord(string $username): array
    {
        $query = '
            select username, to_char(create_date, \'YYYY-MM-DD\') as create_date, create_username, 
                    to_char(expiration_date, \'YYYY-MM-DD\') as expiration_date, algorithm, comments 
                from ws_authentication_local_users 
                where username = ?
            ';

        $record = $this->dbh->queryfirstrow_assoc($query, strtolower($username));

        if ($record === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            throw new AuthenticationUserNotFoundException(sprintf('Username not found: %s', strtolower($username)));
        }

        return $record;
    }

    public function createUser()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getData();

        $user = $this->createUserFromModel($data);

        $response->setStatus(App::API_CREATED);
        $response->setPayload($user);

        return $response;
    }

    private function createUserFromModel(array $model): array
    {
        $query = '
            insert into ws_authentication_local_users (username, algorithm, credential_hash, create_date,
                create_username, expiration_date, comments)
                values (?, ?, ?, to_date(?, \'YYYY-MM-DD HH24:MI:SS\'), ?, to_date(?, \'YYYY-MM-DD\'), ?)
        ';

        $expirationDate = empty($model['expirationDate']) ? Carbon::now()->addYear() : Carbon::parse($model['expirationDate']);

        $values = [
            strtolower($model['username']),
            self::AUTHENTICATION_USER_PASSWORD_HASH_MD5,
            $this->hashPassword($model['password']),
            Carbon::now()->format('Y-m-d H:i:s'),
            strtoupper($model['creatorId']),
            $expirationDate->format('Y-m-d'),
            $model['comments'],
        ];

        $this->dbh->perform($query, $values);

        return $this->getUserModel($values[0]);
    }

    public function updateUser()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $username = $request->getResourceParam('username');
        $model = $request->getData();

        if (empty($model['username']) || strtolower($model['username']) !== strtolower($username)) {
            throw new AuthenticationUserMismatchException('URL and data usernames do not match');
        }

        try {
            $this->updateUserRecord($model);
        } catch (AuthenticationUserNotFoundException $e) {
            $response->setStatus(App::API_NOTFOUND);
        }

        return $response;
    }

    /**
     * @param array $model
     * @throws AuthenticationUserNotFoundException
     */
    private function updateUserRecord(array $model): void
    {
        $changes = [];

        if (!empty($model['expirationDate'])) {
            $changes[] = [
                'expression' => 'expiration_date = to_date(?, \'YYYY-MM-DD\')',
                'value' => Carbon::parse($model['expirationDate'])->format('Y-m-d')
            ];
        }

        if (empty($changes)) {
            return;
        }

        $values = [];
        $updates = implode(', ', array_reduce($changes, function ($c, $change) use (&$values) {
            $c[] = $change['expression'];
            $values[] = $change['value'];
            return $c;
        }, []));

        $query = "
            update ws_authentication_local_users set $updates 
                where username = ?
        ";

        $this->dbh->perform($query, array_merge($values, [strtolower($model['username'])]));
    }

    public function updateUserPassword()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $username = $request->getResourceParam('username');
        $model = $request->getData();

        if (empty($model['username']) || strtolower($model['username']) !== strtolower($username)) {
            throw new AuthenticationUserMismatchException('URL and data usernames do not match');
        }

        try {
            $this->updateUserRecordPassword($model);
        } catch (AuthenticationUserNotFoundException $e) {
            $response->setStatus(App::API_NOTFOUND);
        }

        return $response;
    }

    /**
     * @param array $model
     * @throws AuthenticationUserNotFoundException
     */
    private function updateUserRecordPassword(array $model): void
    {
        $current = $this->getUserRecord($model['username']);

        $query = '
            update ws_authentication_local_users set credential_hash = ?
                where username = ?
        ';

        $this->dbh->perform($query, [$this->hashPassword($model['password'], $current['algorithm']), strtolower($model['username'])]);
    }

    public function deleteUser()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $username = $request->getResourceParam('username');

        $this->deleteUserRecord($username);

        return $response;
    }

    private function deleteUserRecord(string $username): void
    {
        $query = 'delete from ws_authentication_local_users where username = ?';

        $this->dbh->perform($query, strtolower($username));
    }

    private function hashPassword(string $plainText, int $method = 1): string
    {
        if ($method === self::AUTHENTICATION_USER_PASSWORD_HASH_MD5) {
            return hash('md5', $plainText);
        }

        throw new UnkownHashAlgorithmException(sprintf('Unknown local user hash algorithm: %s', $method));
    }
}
