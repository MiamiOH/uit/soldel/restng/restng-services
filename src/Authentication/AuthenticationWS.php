<?php

namespace MiamiOH\RESTng\Service\Authentication;

class AuthenticationWS extends \MiamiOH\RESTng\Service
{

    private $restService;

    public function setRestService($client)
    {
        $this->restService = $client;
    }

    public function getRestService()
    {
        return $this->restService;
    }

    public function authenticateUser()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getData();

        if (!(isset($data['type']) && $data['type'])) {
            throw new \Exception(__CLASS__ . '::authenticateUser() requires type');
        }

        if (!in_array($data['type'], array('usernamePassword', 'cas-proxy'))) {
            throw new \Exception(__CLASS__ . '::authenticateUser() type must be one of "usernamePassword" or "cas-proxy"');
        }

        /** @var \GuzzleHttp\Client $client */
        $client = $this->restService->newClient(array('base_uri' => env('MU_API_URL', 'https://example.com')));
        $postUrl = '/api/authentication/v1';

        $authResponse = json_decode($client->post($postUrl, [
            'http_errors' => false,
            'json' => $data,
        ])->getBody(), true);

        $response->setStatus($authResponse['status']);
        $response->setPayload($authResponse['data']);

        return $response;
    }

    public function validateToken()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $token = $request->getResourceParam('token');

        $client = $this->restService->newClient(array('base_uri' => env('MU_API_URL', 'https://example.com')));
        $getUrl = '/api/authentication/v1/' . urlencode($token);

        $authResponse = json_decode($client->get($getUrl)->getBody(), true);
        $tokenInfo = $authResponse['data'];

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($tokenInfo);

        return $response;
    }
}
